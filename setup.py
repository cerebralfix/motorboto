#!/usr/bin/env python

from setuptools import setup

setup(name='motorboto',
      version='0.1.1',
      description='Get stuff done fast on AWS',
      author='Robert Lechte aka djrobstep',
      author_email='robert@yaks.co.nz',
      url='https://bitbucket.org/cerebralfix/motorboto',
      license='Unlicense',
      install_requires=['boto','futures','netaddr'],
      packages=[
         'motorboto',
      ],
      classifiers=['Development Status :: 3 - Alpha'],
      scripts=[],
)

