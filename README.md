motorboto
=========

Motorboto is a layer on top of [boto](https://github.com/boto/boto) to help get certain common tasks done faster. You might find it useful. 

Motorboto is released as public domain source code by [Cerebralfix](http://cerebralfix.com/), and was written by Robert Lechte (djrobstep on bitbucket and github).

**Warning**: This is a rough release of very alpha software. Use at your own risk. API, code, tests, and documentation are in varying states of completeness and subject to change at any time. Contributions are welcome, developer-focussed documentation will be provided soon. 

**Warning 2**: Also note that using this software or even running the tests included with this project requires an AWS account, will mess with your AWS setup, and also make you incur charges on AWS. Only run this code and/or tests if you know what you are doing and accept the risks.

Getting Started
---------------

Connect like this:

    c = motorboto.AWS(id, key)

or if you have a `~/.boto` credentials file just do:

    c = motorboto.AWS()

and motorboto will use those credentials.

S3
--

Working with buckets and keys looks like this:

	>>> bucket = c.s3.buckets['my_bucket']
	>>> key = bucket.keys['/keyname']
	>>> key.save('path/to/local/file.txt')

or more succinctly:

	>>> c.s3.buckets['my_bucket'].keys['/keyname'].save('path/to/local/file.txt)

If your key/bucket doesn't exist already, motorboto will create it for you, so make sure you have your names correct!

When you save a key, motorboto is smart enough to only reupload it if the file contents have changed. Saving a key twice:

	>>> key.save('path/to/local/file.txt')
	'SET FROM FILE'
	>>> key.save('path/to/local/file.txt')
	'UNCHANGED'

will only result in one upload. Motorboto uses (parallel) multipart uploads for larger files, so if your upload gets interrupted, you won't need to start over.

Motorboto can also help you avoid wasteful reuploading by allowing you to specify a checksum -> (bucket, keyname) dictionary of keys to copy from. If there is an existing key in any of those buckets with the same data (as determined by md5 checksum) as the one you're saving, it'll be copied from that key on s3's servers instead of being reuploaded. Much faster! 

For example, if you want use a key's own bucket to copy from, and additionally a subset of another bucket, you would do this:

	>>> bucket1_keys = k.bucket.keys
	>>> bucket2_keys = bucket2.find_keys(prefix='/subfolder/')
	>>> existing_keys = bucket1_keys.update(bucket2_keys)
	>>> existing_checksums = motorboto.s3.keys_by_checksum(existing_keys)
	>>> k.save(copy_from=existing_checksums)

You can also create checksum-based key names:

	>>> k = bucket.get_checksum_key('path/to/local/file.txt')
	>>> k.name
	'fef516e601ce85a158207f7b881ed085.txt'
	>>> k.save()
	>>> k = bucket.get_checksum_key('path/to/local/file.txt')
	>>> k.name
	'fef516e601ce85a158207f7b881ed085.txt'
	>>> k.save()

This handy if you're storing your web assets on s3 and you want your website assets to change path every time they get updated to avoid browser cache invalidation issues, or if you're backing up many copies of identical files and you only want to store a single copy.

It's common that you want to sync a local folder up to s3. Motorboto makes that easy.

	>>> keys = b.keys_for_folder('./assets/')
	>>> keys.save(public=True)

This saves in parallel.

While you're doing this you might want to add caching headers to each of your keys, and give them checksum-based names, and put them in a version folder. That looks like this:

	>>> keys = b.keys_for_folder('./assets/', as_checksums=True, into_subfolder='/version527')
	>>> for k in keys: keys[k].metadata['Cache-Control'] = 'max-age:3000'
	>>> keys.save(public=True)

And if you want to copy data from one key to another, you don't need to reupload it.

EC2
---

Instances security groups are where you'd expect them to be:

    >>> region = c.ec2['region-name-1']
    >>> region.instances
    >>> region.security_groups

More EC2 documentation coming soon :)

