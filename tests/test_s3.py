import unittest
import sys, os, yaml, time, random, string, ConfigParser, tempfile
import boto

import logging
logging.basicConfig(level=logging.INFO)

TEST_DIR = os.path.dirname(__file__)
sys.path.insert(0, os.path.abspath(os.path.join(TEST_DIR,'..')))
import motorboto

CONFIG_FILENAME = os.path.expanduser('~/.boto')
TEST_UPLOAD_FOLDER = os.path.join(TEST_DIR,'s3_test_files')
TEST_UPLOAD_FOLDER_TRAILING_SLASH = TEST_UPLOAD_FOLDER + os.sep
TEST_BUCKET_NAME = 'testonly-motorboto'

TEST_FILE_1 = os.path.join(TEST_UPLOAD_FOLDER,'test_file_1.txt')
TEST_FILE_2 = os.path.join(TEST_UPLOAD_FOLDER,'test_file_2.txt')

def md5(fname):
    with open(fname, 'rb') as f:
        return boto.s3.key.Key().compute_md5(f)[0]


def random_bucket_name():
    return "%s-%s" % (TEST_BUCKET_NAME, 
                     random.randint(1000,10000))

class TestConnect(unittest.TestCase):
    def test_connect_s3(self):
        config = ConfigParser.SafeConfigParser()
        config.read(CONFIG_FILENAME)
        
        id = config.get('Credentials','aws_access_key_id')
        key = config.get('Credentials','aws_secret_access_key')
                
        c = motorboto.AWS(id, key)
        c2 = motorboto.AWS()
        
        self.assertIsInstance(c.s3, 
                              motorboto.s3.Connection, 
                              "connection created")


class TestS3(unittest.TestCase):

    def setUp(self):
        c = motorboto.AWS()
        self.s3 = c.s3
        self.bucket = self.s3.buckets[TEST_BUCKET_NAME]
        
    def tearDown(self):
        pass
    
    def test_bucket_subscripting(self):
        self.assertEqual(self.bucket.name, self.s3.buckets[self.bucket.name].name)
    
    def test_bucket_list(self):
        buckets = self.s3.buckets
        for b in buckets.values():
            #assert isinstance(b, motorboto.s3.Bucket)
            self.assertIsInstance(b, motorboto.s3.Bucket)

    @unittest.skip("aws doesn't create/delete buckets in a timely fashion")
    def test_create_delete(self):
        temp_bucket_name = random_bucket_name()
        bucket = self.s3.get_or_create_bucket(temp_bucket_name)
        DELAY = 30

        time.sleep(DELAY)
        bucket.delete()
        
    def test_basic_save(self):
        bucket = self.bucket
    
        for k in bucket.keys.values():
            k.delete()

        k1 = bucket.keys['x']
        k2 = bucket.get_key('y')
        
        path1 = TEST_FILE_1
        path2 = TEST_FILE_2
        
        k1.save(fname=path1)
        k2.save(path2)
        
        
    def test_save(self):
        bucket = self.bucket
        for k in bucket.keys.values():
            k.delete()

        key = bucket.keys['test_key.txt']
        self.assertFalse(key.exists)

        modified = key.save(TEST_FILE_1)
        self.assertTrue(key.exists)
        self.assertTrue(modified)
        bucket.get_key('test_key.txt')
        self.assertTrue(key.exists)

        key = bucket.get_key('test_key.bad')
        self.assertFalse(key.exists)
        
        checksum_key = bucket.get_checksum_key(TEST_FILE_1)
        checksum_key.save()
        keystring = checksum_key.name
        key = bucket.get_key(keystring)
        self.assertTrue(key.exists)
        
        checksum_key = bucket.get_checksum_key(TEST_FILE_1, in_subfolder='xyz')
        checksum_key.save()
        keystring = checksum_key.name
        key = bucket.get_key(keystring)
        self.assertTrue(key.exists)
    
        key = bucket.keys['new_key']
        existing = bucket.keys.by_checksum
        result = key.save(TEST_FILE_1, copy_from=existing)
        self.assertEqual(result, 'SET FROM EXISTING')

        self.assertTrue(key.exists)

        self.assertEqual(key.server_checksum, md5(TEST_FILE_1))

    def test_big_save(self):
        bucket = self.bucket
        for k in bucket.keys.values():
            k.delete()
        return
        with tempfile.NamedTemporaryFile(delete=False) as f:
            try:
                kilobyte_string = ''.join( [random.choice(string.letters) 
                                            for i in xrange(1024)] )
                for i in range(1024 * 11): # 11 megabyte file
                    f.write(kilobyte_string)
                f.flush()
                f.close()
                
                key = bucket.keys['bigass_file']
                key.metadata['x'] = 'y'
                result = key.save(f.name)
                self.assertEqual(result, 'SET FROM MULTIPART')
                self.assertEqual(key.server_checksum, md5(f.name))
                
                k2 = bucket.keys['bigass_file2']

                existing = bucket.keys.by_checksum

                result = k2.save(f.name, copy_from=existing)

                self.assertEqual(result, 'SET FROM EXISTING')

                self.assertEqual(key.size, k2.size)
                
            finally:
                os.unlink(f.name)
       
    def test_metadata(self):
        b = self.bucket
        
        k = b.keys['testmetadata.txt']
        k.save(TEST_FILE_1)
        k.metadata['Cache-Control'] = 'max-age:3000'
        k.save(TEST_FILE_1)
        
        k = b.get_key('testmetadata.txt')
        
        k.metadata['Cache-Control'] = 'max-age:4000'
        k.metadata['rating'] = 'x'
        k.fname = TEST_FILE_1
        k.save()
        k = b.get_key('testmetadata.txt')
        k.metadata['testentry'] = 'true'
        k.save(TEST_FILE_1)
        

if __name__ == '__main__':
    unittest.main()
    