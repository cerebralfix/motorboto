import unittest
import sys, os, yaml, time, random, string
import boto

import logging
logging.basicConfig(level=logging.WARNING)
logging.getLogger('boto').setLevel(logging.ERROR)

TEST_DIR = os.path.dirname(__file__)
sys.path.insert(0, os.path.abspath(os.path.join(TEST_DIR,'..')))
import motorboto

TEST_REGION = 'ap-southeast-2'
TEST_SGNAME = 'motorboto-testonly-sg'
TEST_KPNAME = 'motorboto-testonly-kp'

class TestSecurityGroups(unittest.TestCase):

    def setUp(self):
        c = motorboto.AWS()
        self.r = c.ec2.regions[TEST_REGION]
        self.r.security_groups[TEST_SGNAME].delete()
        
    def tearDown(self):
        self.r.security_groups[TEST_SGNAME].delete()
    
    def test_create_delete(self):
        r = self.r
        self.assertFalse(TEST_SGNAME in r.security_groups)
        sg = r.security_groups[TEST_SGNAME]
        self.assertFalse(TEST_SGNAME in r.security_groups)
        
        sg.save()
        self.assertTrue(TEST_SGNAME in r.security_groups)
        
        sg.delete()
        self.assertFalse(TEST_SGNAME in r.security_groups)
        
    def test_rules(self):
        import netaddr
        r = self.r
        sg = r.security_groups[TEST_SGNAME]
        sg.rules[('icmp',-1,-1)] = [netaddr.IPNetwork('8.8.8.8/32')]
        sg.save()
        
        sg2 = r.security_groups[TEST_SGNAME]
        self.assertEqual(sg2.rules[('icmp',-1,-1)], sg.rules[('icmp',-1,-1)])
        self.assertEqual(sg2.rules[('icmp',-1,-1)], set([netaddr.IPNetwork('8.8.8.8/32')]))

    def test_get_instances(self):
        r = self.r
        sg = r.security_groups[TEST_SGNAME]
        
        instances = sg.instances
        
        for i in instances:
            self.assertIsInstance(i, motorboto.ec2.Instance)
            self.assertEqual(sg.name, i.security_group.name)
        
class TestInstances(unittest.TestCase):
    def setUp(self):
        c = motorboto.AWS()
        self.r = c.ec2.regions[TEST_REGION]
        self.sg = self.r.security_groups[TEST_SGNAME]
        self.sg.save()
        kps = self.r.key_pairs
        try:
            self.kp = kps[TEST_KPNAME]
        except KeyError:
            self.kp = self.r.new_key_pair(TEST_KPNAME, save_folder=None)
            
        for i in self.sg.instances:
            i.terminate()
        
    def tearDown(self):
        for i in self.sg.instances:
            i.terminate()

    def test_new(self):
        r = self.r
        i = r.new_instance('test1', security_group=self.sg, key_pair=self.kp)
        self.assertEqual(i.name, 'test1')
        
        i = r.new_instance('test2', security_group=self.sg, key_pair=self.kp, wait=True)
        self.assertEqual(i.name, 'test2')
        self.assertEqual(i.state, 'running')
        
    
