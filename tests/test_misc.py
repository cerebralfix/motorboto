import unittest
import sys, os, yaml, time, random, string, ConfigParser, tempfile
import boto

import logging
logging.basicConfig(level=logging.INFO)

TEST_DIR = os.path.dirname(__file__)
sys.path.insert(0, os.path.abspath(os.path.join(TEST_DIR,'..')))
import motorboto

CONFIG_FILENAME = os.path.expanduser('~/.boto')
TEST_FOLDER_NAME = 's3_test_files'
TEST_UPLOAD_FOLDER = os.path.join(TEST_DIR, TEST_FOLDER_NAME)
TEST_UPLOAD_FOLDER_TRAILING_SLASH = TEST_UPLOAD_FOLDER + os.sep

TEST_FILE_1 = os.path.join(TEST_UPLOAD_FOLDER,'test_file_1.txt')
TEST_FILE_2 = os.path.join(TEST_UPLOAD_FOLDER,'test_file_2.txt')

TEST_BUCKET_NAME = 'testonly-motorboto'

def test_manifests():
	b = motorboto.AWS().s3.buckets[TEST_BUCKET_NAME]
	keys = b.keys_for_folder(TEST_UPLOAD_FOLDER)
	keys.save()

class TestManifests(unittest.TestCase):
    def test_filesinfolder(self):
    	from motorboto.misc import files_in_folder

        without_slash = files_in_folder(TEST_UPLOAD_FOLDER)
        with_slash = files_in_folder(TEST_UPLOAD_FOLDER_TRAILING_SLASH)

        both = zip(with_slash, without_slash)

        for exclude_root, include_root in both:
            self.assertEqual(os.path.join(TEST_FOLDER_NAME,exclude_root.relative),
            				include_root.relative)

        #test default ignore
        relpaths = [f.relative for f in with_slash]
        self.assertFalse('.svn' in relpaths)
        self.assertTrue('.ignoretest' in relpaths)

        custom = files_in_folder(TEST_UPLOAD_FOLDER_TRAILING_SLASH,
        							ignore=['.ignoretest'])

        relpaths = [f.relative for f in custom]
        self.assertTrue('.svn' in relpaths)
        self.assertFalse('.ignoretest' in relpaths)

