
quantal = {
	'ap-northeast-1': 'ami-f8a813f9',
	'ap-southeast-1': 'ami-7d23612f',
	'eu-west-1': 'ami-640a0610',
	'sa-east-1': 'ami-2bec3436',
	'us-east-1': 'ami-7539b41c',
	'us-west-1': 'ami-26745463',
	'ap-southeast-2': 'ami-ef7ee9d5',
	'us-west-2': 'ami-422ea672',
}

precise = {
	'ap-northeast-1': 'ami-c4a912c5',
	'ap-southeast-1': 'ami-01226053',
	'eu-west-1': 'ami-960f03e2',
	'sa-east-1': 'ami-91eb338c',
	'us-east-1': 'ami-fd20ad94',
	'us-west-1': 'ami-b87252fd',
	'ap-southeast-2': 'ami-737ee949',
	'us-west-2': 'ami-7e2da54e',
}

UBUNTU_AMIS = {
	'12.10': quantal,
	'quantal': quantal,
	'12.04': precise,
	'precise': precise,
}
