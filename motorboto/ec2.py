import boto.ec2

import aws
import netaddr
import time
from utils import property_lazy, MappingCustom

CLOUDINIT_USER_DATA = """#cloud-config
mounts:
 - [ ephemeral0, /var/log/ephemeral, auto, "defaults,noexec" ]
        
        """


class Connection(object):
    def __init__(self, aws):
        self.aws = aws

    @property_lazy
    def regions(self):
        c = self
        class RegionsMapping(MappingCustom):
            def __getitem__(self, key):
                return Region(key, c)

            @property_lazy
            def data(self):
                return [r.name for r in boto.ec2.regions()]

            def refresh():
                del self.data

        return RegionsMapping()

    def _connect(self, region_name):
        return boto.ec2.connect_to_region(region_name, **self.aws._auth_params)

class Region(object):
    #contains a region connection
    def __init__(self, name, connection):
        self.name = name
        self.connection = connection

    @property_lazy
    def boto(self):
        return self.connection._connect(self.name)

    @property
    def security_groups(self):
        r = self
        class SecurityGroupsMapping(dict):
            def __missing__(self, key):
                return SecurityGroup(r, key)
        return SecurityGroupsMapping({ s.name: SecurityGroup(self, s.name, s)
                for s in self.boto.get_all_security_groups() })

    def _instances(self):
        reservations = self.boto.get_all_instances()
        return [i for r in reservations for i in r.instances]
              

    @property
    def instances(self):
        return [Instance(self, i.id, i) for i in self._instances()]
        
    def state(self):
        return self.boto.state

    def find_instances(self, instance_id=None, security_group_name=None, state=None):
        instances = self._instances()
        if instance_id:
            instances = [i for i in instances if i.id==instance_id]
            
        if security_group_name:
            instances = [i for i in instances if security_group_name 
                            in [g.name for g in i.groups]]
        if state:
            instances = [i for i in instances if i.state==state]
        return [Instance(self, i.id, i) for i in instances]

    @property
    def key_pairs(self):
        return { kp.name: KeyPair(self, kp.name, kp) 
                    for kp in self.boto.get_all_key_pairs() }
        
    def new_key_pair(self, name, save_folder):
        boto_key_pair = self.boto.create_key_pair(name)
        
        if save_folder:
            boto_key_pair.save(save_folder)
            
        return KeyPair(self, boto_key_pair.name, boto_key_pair)
        
    def new_instance(self,
                name,
                security_group,
                key_pair,
                instance_type='t1.micro',
                ami=None,
                delete_on_termination=True,
                ebs_size=10,
                instance_profile=None,
                wait=False):
        if not ami:
            import constants
            ami = constants.UBUNTU_AMIS['quantal'][self.name]
        
        #custom ebs size and termination policy
        import boto.ec2.blockdevicemapping
        bdt = boto.ec2.blockdevicemapping.BlockDeviceType(connection=self.boto)
        bdt.size = ebs_size
        bdt.delete_on_termination = delete_on_termination
        bdm = boto.ec2.blockdevicemapping.BlockDeviceMapping(connection=self.boto)
        bdm['/dev/sda1'] = bdt
        
        reserv = self.boto.run_instances(ami,
                                security_groups=[security_group.name],
                                instance_type=instance_type,
                                key_name=key_pair.name,
                                block_device_map=bdm,
                                instance_profile_name=instance_profile,
                                user_data=CLOUDINIT_USER_DATA)
                                
        [i.add_tag('Name',name) for i in reserv.instances]
        i.update()
        
        instance = Instance(self, i.id, i)
        if wait:
            instance.wait(state='running')
        return instance
        

class KeyPair(object):
    def __init__(self, region, name, boto):
        self.region = region
        self.name = name
        self.boto = boto
        
    def delete(self):
        self.boto.delete()

class Instance(object):
    def __init__(self, region, iid, boto):
        self.region = region
        self.id = iid
        self.boto = boto
            
    def wait(self, state):
        while self.state != state:
            time.sleep(5)
            self.refresh()
    
    def refresh(self):
        self.boto.update()
    
    @property
    def state(self):
        return self.boto.state
    
    @property
    def name(self):
        return self.boto.tags.get('Name', None)
    
    @property
    def public_dns(self):
        return self.boto.public_dns_name
        
    def terminate(self,wait=False):
        self.boto.terminate()
        self.refresh()
        if wait:
            self.wait(state='terminated')
    
    @property
    def security_group(self):
        boto_sg = self.boto.groups[0]
        return SecurityGroup(self.region, boto_sg.name, boto_sg)
        
class Volume(object):
    def take_snapshot(self):
        pass

class Snapshot(object):
    @property
    def volume(self):
        return None

class SecurityGroup(object):
    def __init__(self, region, name, boto=None):
        self.region = region
        self.name = name
        if boto:
            self.boto = boto

    def __eq__(self, other):
        return isinstance(other, self.__class__) \
            and self.region==other.region and self.name==other.name
    
    def __hash__(self):
        return hash((self.__class__, self.region.name, self.name))
    
    def __ne__(self, other):
        return not self.__eq__(other)

    @property_lazy
    def boto(self):
        sgs = { sg.name: sg for sg in self.region.boto.get_all_security_groups() }
        try:
            return sgs[self.name]
        except KeyError:
            import datetime
            d = datetime.datetime.utcnow()
            description = "Created by motorboto on %s" % d
            return self.region.boto.create_security_group(self.name, description)

    @property 
    def id(self):
        return self.boto.id

    @property
    def exists(self):
        return self.name in self.region.security_groups

    def save(self):
        self.boto
        self.rules.save()
        
    def delete(self):
        if self.exists:
            self.region.boto.delete_security_group(self.name)

    @property_lazy
    def rules(self):
        sg = self

        def _grant(grant):
            if grant.cidr_ip:
                return netaddr.IPNetwork(grant.cidr_ip)
            else:
                return SecurityGroup(sg.region, grant.name)

        def _authorize(k, grants):
            i, f, t = k
            for g in grants:
                if isinstance(g, SecurityGroup):
                    self.boto.authorize(ip_protocol=i, 
                            from_port=f, 
                            to_port=t, 
                            src_group=g.boto)
                else:
                    self.boto.authorize(ip_protocol=i, 
                                    from_port=f, 
                                    to_port=t, 
                                    cidr_ip=str(g))

        def _revoke(k, grants):
            i, f, t = k
            for g in grants:
                if isinstance(g, SecurityGroup):
                    self.boto.revoke(ip_protocol=i, 
                            from_port=f, 
                            to_port=t, 
                            src_group=g.boto)
                else:
                    self.boto.revoke(ip_protocol=i, 
                                    from_port=f, 
                                    to_port=t, 
                                    cidr_ip=str(g))

        class SecurityGroupRulesMapping(dict):
            def __setitem__(self, key, value):
                super(SecurityGroupRulesMapping, self).__setitem__(key, set(value))
            def save(self):
                existing = SecurityGroup(sg.region, sg.name).rules
                existing_keys = set(existing.keys())
                new_keys = set(self.keys())

                added = set(new_keys) - set(existing_keys)
                removed = set(existing_keys) - set(new_keys)
                common = set(new_keys) & set(existing_keys)

                for k in added:
                    _authorize(k, self[k])

                for k in removed:
                    _revoke(k, self[k])

                for k in common:
                    after = self[k]
                    before = existing[k]
                    for add in set(after) - set(before):
                        _authorize(k, [add]) 
                    for remove in set(before) - set(after):
                        _revoke(k, [remove])

        d = {(i.ip_protocol, int(i.from_port), int(i.to_port)): 
                set([_grant(g) for g in i.grants])
                for i in self.boto.rules }
        return SecurityGroupRulesMapping(d)
    
    @property
    def instances(self):
        return self.region.find_instances(security_group_name=self.name)
    
