import collections
from contextlib import contextmanager

class property_lazy(object):
    '''
    meant to be used for lazy evaluation of an object attribute.
    property should represent non-mutable data, as it replaces itself.
    '''
    def __init__(self,fget):
        self.fget = fget
        self.func_name = fget.__name__

    def __get__(self,obj,cls):
        if obj is None:
            return None
        value = self.fget(obj)
        setattr(obj,self.func_name,value)
        return value

class MappingCustom(collections.Mapping):
	def __getitem__(self, key):
		return self.data[key]

	def __iter__(self):
	    return iter(self.data)

	def __contains__(self, key):
	    return key in self.data

	def __len__(self):
	    return len(self.data)
		

@contextmanager
def stringasnamedtemporaryfile(s):
    import tempfile, os
    with tempfile.NamedTemporaryFile(delete=False) as f:
        try:
            f.write(s)
            f.flush()
            f.close()
            yield f
        finally:
            os.unlink(f.name)
