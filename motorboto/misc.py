import json
import os
import posixpath

import collections

class manifests(object):
    """Convenience methods for creating lists of folder contents."""
    @staticmethod
    def filetree_json(keys):
        d = {}

        for kname, k in keys.items():
            folders, fname = posixpath.split(kname)
            
            leaf = d
            for part in folders.split(posixpath.sep):
                if not part:
                    continue
                if part not in leaf:
                    leaf[part] = {}
                leaf = leaf[part]
            leaf[fname] = [k.local_checksum, k.local_size]
        return d
    @staticmethod
    def checksums_json(keys):
        """Creates a flat manifest object of local names -> key names. Useful if you're using checksum-based key names.
        """
        return {kname: k.local_checksum for kname, k in keys.items() }



Filename = collections.namedtuple('Filename','absolute,relative,posixslashes')

def iterfiles_in_folder(folderpath, ignore=('.svn', '.git', '.hg')):
    #print folderpath
    #print ignore
    #print ignore
    if not os.path.isdir(folderpath):
        raise ValueError("folder doesn't exist")
    if not isinstance(ignore, collections.Iterable):
        raise ValueError("ignore must be an iterable list of names to ignore")

    for dirpath, dirs, files in os.walk(folderpath): 
        for ignore_name in ignore:
            if ignore_name in dirs:
                dirs.remove(ignore_name)
            if ignore_name in files:
                files.remove(ignore_name)
        for f in files:
            absolute = os.path.join(dirpath, f)
            head, _ = os.path.split(folderpath)
            relative = os.path.relpath(os.path.join(dirpath, f), head)
            splitpath = relative.split(os.sep)
            posixslashes = posixpath.join(*splitpath)
            yield Filename(absolute,relative,posixslashes)

def files_in_folder(folderpath, ignore=('.svn', '.git', '.hg')):
    return [f for f in iterfiles_in_folder(folderpath, ignore=ignore)]
