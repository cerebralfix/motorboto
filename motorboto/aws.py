import boto

class AWS(object):
    def __init__(self, aws_id=None, aws_key=None):
        self.id = aws_id
        self.key = aws_key
        
    @property
    def s3(self):
        import s3
        return s3.Connection(self)
    
    @property
    def cloudfront(self):
        import cloudfront
        return cloudfront.Connection(self)

    @property
    def ec2(self):
        import ec2
        return ec2.Connection(self)

    def get_copy(self):
        return AWS(self.id, self.key)
        
    @property
    def _auth_params(self):
        if self.id and self.key:
            return {'aws_access_key_id': self.id,
                    'aws_secret_access_key': self.key }
        return {}

class Connection(object):
    def __init__(self, aws):
        self.aws = aws
