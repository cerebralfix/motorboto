


class ConnectionCloudFront(aws.Connection):
    connection_method='connect_cloudfront'
    def distributions(self):
        return self.boto.get_all_distributions()
    
    def distribution(self, s3origin):
        distros = self.distributions()
        
        for ds in distros:
            distro = ds.get_distribution()
            if distro.config.origin.dns_name == s3origin:
                return distro