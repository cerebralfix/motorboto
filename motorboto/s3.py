import datetime
import boto, boto.s3.key, boto.s3.bucket
import collections, os, posixpath, copy
import logging, multiprocessing, sys, time, json
from concurrent import futures

import aws
import filechunkio

FIVE_MB = 1024 * 1024 * 5
FIVE_GB = FIVE_MB * 1024

from utils import property_lazy, MappingCustom, stringasnamedtemporaryfile

def local_checksum_tuple(fname):
    with open(fname, 'rb') as f:
        return boto.s3.key.Key().compute_md5(f)

def key_file_pairs(folderpath, ignore=None):
    DEFAULT_IGNORE_LIST = ['.svn', '.git', '.hg']
    if ignore==None:
        ignore = DEFAULT_IGNORE_LIST

    if not os.path.isdir(folderpath):
        raise ValueError("folder doesn't exist")

    for dirpath, dirs, files in os.walk(folderpath): 
        for ignore_name in ignore:
            if ignore_name in dirs:
                dirs.remove(ignore_name)
            if ignore_name in files:
                files.remove(ignore_name)
                
        for f in files:
            file_path = os.path.join(dirpath, f)
            head, _ = os.path.split(folderpath)
            key_path = os.path.relpath(os.path.join(dirpath, f), head)
            splitpath = key_path.split(os.sep)
            key_path = posixpath.join(*splitpath)
            yield (key_path, file_path)

class KeysMapping(MappingCustom):
    def __init__(self, b, f=None):
        self.b = b
        self.f = f

    def __getitem__(self, key):
        if 'data' in self.__dict__:
            try:
                return self.data[key]
            except KeyError:
                pass
        return Key(self.b, key)

    @property_lazy
    def data(self):
        if not self.f:
            return {}
        return self.f()

    def refresh(self):
        del self.data

    @property
    def by_checksum(self):
        return { k.server_checksum: (k.bucket.name, k.name)
                                    for k in self.values() }

    def save(self, copy_from=None, public=False, parallel=True):
        changes = collections.Counter()

        if parallel:
            with futures.ThreadPoolExecutor(max_workers=128) as executor:
                future_to_key = \
                    { executor.submit(k.save,
                                      threadsafe=True, 
                                      copy_from=copy_from,
                                      public=public):
                     k for k in self.values() }
                     
                for future in futures.as_completed(future_to_key):
                    k = future_to_key[future]
                    result = future.result()
                    logging.info('SAVED: %s (%s)' % (k.name, result))
                    changes[result] += 1
        else:
            for k in self.values():
                result = k.save(copy_from=copy_from, public=public)
                logging.info('SAVED: %s (%s)' % (k.name, result))
                changes[result] += 1
        self.refresh()
        return changes


class Connection(aws.Connection):
    @property_lazy
    def boto(self):
        return boto.connect_s3(**self.aws._auth_params)

    @property_lazy
    def buckets(self):
        c = self
        class BucketsMapping(MappingCustom):
            def __getitem__(self, key):
                if 'data' in self.__dict__:
                    try:
                        return self.data[key]
                    except IndexError:
                        pass
                return Bucket(c, key)

            @property_lazy
            def data(self):
                return { bb.name: Bucket(c, bb)
                        for bb in c.boto.get_all_buckets() }

            def refresh():
                del self.data
        return BucketsMapping()

    def refresh(self):
        if 'boto' in self.__dict__:
            del self.boto
        if 'buckets' in self.__dict__:
            del self.buckets

class Bucket(object):
    def __init__(self, 
                 connection=None, 
                 bucket=None):
        self.connection = connection

        if isinstance(bucket, boto.s3.bucket.Bucket):
            self.name = bucket.name
            self.boto = bucket
        else:
            self.name = bucket

    #this deletes the boto object if pickled
    def __getstate__(self):
        odict = self.__dict__.copy() 
        odict['boto'] = None
        return odict
    
    @property
    def exists(self):
        if 'data' in self.__dict__:
            return True
        bb = self._get_boto_bucket()
        if bb:
            self.boto = bb
            return True
        return False

    def _get_boto_bucket(self, create_if_nonexistent=False):
        try:
            return self.connection.boto.get_bucket(self.name)
        except boto.exception.S3ResponseError:
            pass
        if create_if_nonexistent:
            return self.connection.boto.create_bucket(self.name)
            
        return None

    @property_lazy
    def boto(self):
        try:
            return self._get_boto_bucket(create_if_nonexistent=True)
        except boto.exception.S3ResponseError:
            raise ValueError('bucket name already taken')
    
    def create(self):
        newb = self.connection.boto.create_bucket(self.name)
        self.connection.buckets.refresh()
        return newb
    
    @property_lazy
    def keys(self):
        return KeysMapping(self, lambda: self._find_keys())

    def find_keys(self, prefix='', delimiter=''):
        return KeysMapping(self, lambda: self._find_keys(prefix,delimiter))

    def _find_keys(self, prefix='', delimiter=''):
        return { k.name: Key(self, k) for k in self.boto.list(prefix='', delimiter='') }

    def delete(self):
        for k in self.keys:
            k.delete()
        self.boto.delete()
        
    def get_key(self, key_name, in_subfolder=None, fname=None):
        key_name = posixpath.join(in_subfolder or '', key_name)
        return Key(self, key_name, fname)

    def get_checksum_key(self, fname, in_subfolder=None):
        local_checksum = local_checksum_tuple(fname)[0]
        
        key_name = local_checksum + os.path.splitext(fname)[1]
        key = self.get_key(key_name, in_subfolder=in_subfolder)
        key.fname = fname
        return key

    def keys_for_folder(self,
                        folder_path,
                        in_subfolder=None,
                        as_checksums=False):
        return KeysMapping(self, lambda: self._keys_for_folder(folder_path,
                                    in_subfolder, as_checksums))

    def _keys_for_folder(self,
                        folder_path,
                        in_subfolder=None,
                        as_checksums=False):
        import misc
        filenames = misc.files_in_folder(folder_path)
        if not as_checksums:
            keys = [self.get_key(kpath,
                                 in_subfolder=in_subfolder,
                                 fname=fpath)
                    for fpath, _, kpath in filenames]
        else:
            keys = [self.get_checksum_key(fpath,
                                          in_subfolder=in_subfolder)
                    for fpath,_,_  in pairs]
        return { k.name: k for k in keys }
    
    def status_multipart(self):
        return [mp for mp in self.boto.list_multipart_uploads()]

    def cleanup_multipart(self):
        for mp in self.status_multipart():
            mp.cancel_upload()
    
    def refresh(self):
        if 'boto' in self.__dict__:
            del self.boto
        if 'keys' in self.__dict__:
            del self.keys
        self.connection.refresh()
    
class Key(object):
    def __init__(self, 
                 bucket, 
                 keyname_or_boto_key, 
                 fname=None):
        self.bucket = bucket
        self.fname = fname
        
        if isinstance(keyname_or_boto_key, boto.s3.key.Key):
            self.boto = keyname_or_boto_key
            self.name = keyname_or_boto_key.name
        else:
            tokens = keyname_or_boto_key.split(os.sep)
            self.name = posixpath.join(*tokens)
        self._metadata_from_boto = False
        
    def __eq__(self, other):
        return (isinstance(other, self.__class__)
            and self.name == other.name)
        
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __repr__(self):
        return self.name
    
    def _threadsafe_copy(self):
        k = copy.deepcopy(self)
        k.refresh()
        return k
    
    @property_lazy
    def boto(self):
        _boto = self.bucket.boto.get_key(self.name)
        
        if not _boto:
            _boto = boto.s3.key.Key(self.bucket.boto)
            _boto.key = self.name
        else:
            #get_key returned a full object so the metadata is populated
            self._metadata_from_boto = True
        return _boto
    
    @property
    def exists(self):
        return self.boto.exists()

    @property
    def server_checksum(self):
        if not self.boto.etag:
            return None
        etag = self.boto.etag.strip('"')
        if '-' in etag:
            return self.metadata.get('md5')
        return etag

    @property
    def size(self):
        return self.boto.size

    @property_lazy
    def metadata(self):
        def add_special(m):
            """
            disgusting hack so that these two headers are in the
            metadata dictionary with all the other metadata
            must be a better way to solve this
            """
            if self.boto.cache_control:
                m['Cache-Control'] = \
                        self.boto.cache_control
            if self.boto.content_disposition:
                m['Content-Disposition'] = \
                        self.boto.content_disposition
        
        if 'boto' in self.__dict__ and not self._metadata_from_boto:
            del self.boto
        
        _metadata = self.boto.metadata.copy()
        add_special(_metadata)
        self._current_metadata = _metadata.copy()
        return _metadata

    @property
    def local_checksum(self):
        return local_checksum_tuple(self.fname)[0]

    @property
    def local_size(self):
        return int(os.path.getsize(self.fname))

    def _save(self,
              copy_from=None):
        checksum_tuple = local_checksum_tuple(self.fname)
        local_checksum = checksum_tuple[0]
        logging.info('SAVING: %s as %s' % (self.fname, self.name))
        
        if local_checksum != self.server_checksum:
            self.metadata['md5'] = local_checksum
            if copy_from:
                from_key = copy_from.get(local_checksum)

                if from_key:
                    from_b, from_k = from_key
                    try:
                        self._copy_from_keyname(from_b, from_k, metadata=self.metadata)
                        return 'SET FROM EXISTING'
                    except ValueError:
                        logging.info('could not do a copy, reuploading...')

            MAX_UPLOAD_CHUNK_KB = FIVE_MB
            if os.path.getsize(self.fname) > MAX_UPLOAD_CHUNK_KB:
                logging.info('DOING MULTIPART UPLOAD')
                
                mp = self.bucket.boto.initiate_multipart_upload(self.name,
                                                                metadata=self.metadata)
                fps = filechunkio.chunk_fps(self.fname, MAX_UPLOAD_CHUNK_KB)
                
                with futures.ThreadPoolExecutor(max_workers=128) as executor:
                    future_to_key = {executor.submit(mp.upload_part_from_file,
                                                     fp, index+1): index+1 
                                     for index, fp in enumerate(fps) }
            
                for future in futures.as_completed(future_to_key):
                    index = future_to_key[future]
                    logging.info('SAVED: %s (chunk #%s)' % (self.name, index))
                for part in mp:
                    logging.info('part %s %s' % (part.part_number, part.size))
                mp.complete_upload()
                status = 'SET FROM MULTIPART'
            else:
                for k, v in self.metadata.items():
                    self.boto.set_metadata(k, v)
                self.boto.set_contents_from_filename(self.fname, md5=checksum_tuple)
                status = 'SET FROM FILE'
            return status
        else:
            if self.metadata != self._current_metadata:
                self.bucket.boto.copy_key(self.name, 
                                          self.bucket.name,
                                          self.name,
                                          metadata=self.metadata)
                return 'SET METADATA ONLY'
            else:
                return 'UNCHANGED'
            
    def save(self, 
             fname=None, 
             copy_from=None, 
             public=False,
             threadsafe=False):
        
        self.fname = fname or self.fname
        if not self.fname:
            raise ValueError("you haven't specified a file to save")
        
        k = self._threadsafe_copy() if threadsafe else self
        MAX_RETRIES = 3
        result = None
        for attempt in range(MAX_RETRIES):
            try:
                result = k._save(copy_from)
                
                if public:
                    k.make_public()
                
                break
            except:
                logging.warning('save failed, retrying...')
                time.sleep(attempt * 5)
                k.refresh()
        self.refresh()
        
        result = result or "ERROR, SAVE FAILED"
        return result
    
    def refresh(self):
        if 'boto' in self.__dict__:
            del self.boto
        if 'metadata' in self.__dict__:
            del self.metadata
        self.bucket.refresh()

    def delete(self):
        self.boto.delete()
        self.refresh()
    
    def copy_from_key(self, from_key, metadata=None):
        
        if not from_key.exists:
            raise ValueError("trying to copy from a key that doesn't exist")
        
        self._copy_from_keyname(from_key.bucket.name,
                                from_key.name,
                                metadata,
                                size=from_key.size)

    def _copy_from_keyname(self, 
                            from_bucket_name, 
                            from_key_name, 
                            metadata=None,
                            size=None):
        if not size:
            from_bucket = self.bucket.connection.buckets[from_bucket_name]
            size = from_bucket.keys[from_key_name].size
        ksize = size

        if not ksize:
            raise ValueError("there doesn't seem to be a non-zero sized key here")

        if ksize <= FIVE_GB:
            self.bucket.boto.copy_key(self.name, 
                                      from_bucket_name,
                                      from_key_name,
                                      metadata)
        else:
            mp = self.bucket.boto.initiate_multipart_upload(self.name,
                                                metadata=metadata)

            ranges = [(start, min(start+FIVE_GB, ksize)) 
                    for start in range(0,ksize,FIVE_GB)]
            
            with futures.ThreadPoolExecutor(max_workers=128) as executor:
                future_to_key = {executor.submit(mp.copy_part_from_key,
                                                 from_bucket_name,
                                                 from_key_name,
                                                 index+1,
                                                 r[0], r[1]-1,
                                                 ): index+1
                                 for index, r in enumerate(ranges) }
            for future in futures.as_completed(future_to_key):
                index = future_to_key[future]
                logging.info('COPIED: %s (chunk #%s)' % (self.name, index))
            mp.complete_upload()
        self.refresh()

    def make_public(self):
        self.boto.make_public()
    
